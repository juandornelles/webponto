  	google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
	google.charts.setOnLoadCallback(drawBarchart);


	    function drawBarchart() {
	      var data = google.visualization.arrayToDataTable([
	        ["Employee", "Hours", { role: "style" } ],
	        ["Me", 0.94, "#b87333"],
	        ["Felipe", 0.49, "silver"],	        
	        ["Marcos", 0.49, "silver"],	        
	        ["Alexandre", 0.49, "silver"],	        
	        ["Suellen", 0.49, "silver"],	        
	        ["Juan", 0.49, "silver"],	
	        ["Ivan", 0.49, "silver"],
	        ["Fabio", 0.49, "silver"]
	      ]);

	      var view = new google.visualization.DataView(data);
	      view.setColumns([0, 1,
	                       { calc: "stringify",
	                         sourceColumn: 1,
	                         type: "string",
	                         role: "annotation" },
	                       2]);

	      var options = {
	        title: "Comparision of extrea Hours by employee this month",
	        width: 1200,
	        height: 300,
	        bar: {groupWidth: "95%"},
	        legend: { position: "none" },
	      };

	      var chart = new google.visualization.ColumnChart(document.getElementById("chart_comparision"));
	      chart.draw(view, options);
	  }



	  function drawChart() {
	    var data = google.visualization.arrayToDataTable([
	      ['Day', 'Hours'],
	      ['1',  8],
	      ['2',  8],
	      ['3',  8],
	      ['4',  8],
	      ['5',  8],
	      ['6',  8],
	      ['7',  8],
	      ['8',  8],
	      ['9',  8],
	      ['10',  8],
	      ['11',  9],
	      ['12',  8],
	      ['13',  8],
	      ['14',  10],
	      ['15',  8],
	      ['16',  8],
	      ['17',  8],
	      ['18',  8],
	      ['19',  8],
	      ['20',  8],
	      ['21',  8],
	      ['22',  8],
	      ['23',  8],
	      ['24',  8],
	      ['25',  8],
	      ['26',  8],
	      ['27',  8],
	      ['28',  8],
	      ['29',  8],
	      ['30',  8] 	            
	    ]);

	    var options = {
	      title: 'Extra Hours on Month',
	      curveType: 'function',
	      legend: { position: 'bottom' }
	    };

	    var chart = new google.visualization.LineChart(document.getElementById('chart_individual'));

	    chart.draw(data, options);
	  }



