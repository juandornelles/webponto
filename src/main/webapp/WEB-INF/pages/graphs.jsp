<!DOCTYPE html>

<html>
	<head>		
	    <meta charset="UTF-8">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	    <script type="text/javascript" src="js/loader.js"></script>
	    <script type="text/javascript" src="js/charts.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	</head>

	<body>
		<div class='allbody'>
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      	 <a class="navbar-brand" href="#">Web Ponto</a>
			    </div>
			    <ul class="nav navbar-nav">
				      <li><a href="ponto.jsp">Home</a></li>
				      <li><a href="historico.jsp">History</a></li>
				      <li class="active"><a href="graphs.jsp">Graphs</a></li>
			    </ul>

			  </div>
			</nav>


			<div class="row pontorow">
				<div id="chart_individual" class='graphs' ></div>
  				<div id="chart_comparision" class='graphs' ></div>

			</div>

		</div>
	</body>


</html>


