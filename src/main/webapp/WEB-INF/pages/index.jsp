<!DOCTYPE html>

<html>
	<head>		
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	</head>

	<body>
		<div class='allbody'>


			<div class="row logorow">
				<div class='col-md-offset-5 col-sm-offset-4'>
					<img src='img/logosmall.png'>
					<h2>WebPonto</h2>
				</div>
			</div>

			<div class="row loginrow">
				<div class='col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3' id='loginbox'>
					<form>
						<div class="form-group">
						    <label>Login</label>
							<input class='form-control' type="text" name="login">
						</div>

						<div class="form-group">
						    <label>Password</label>
							<input class='form-control' type="password" name="password">
						</div>

						<div class="form-group">
							<input class='btn btn-primary' type="submit" name="submit" value="Login">
						</div>
					</form>
				</div>

			</div>

		</div>
	</body>

</html>